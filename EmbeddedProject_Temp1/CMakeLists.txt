cmake_minimum_required(VERSION 3.15)

project(EmbeddedProject_Temp1 LANGUAGES C CXX ASM)

find_bsp(
	ID com.sysprogs.arm.renesas
	VERSION 3.3.0
	MCU R7FA4M2AC3CFM
	CONFIGURATION com.sysprogs.bspoptions.cmse=-mcmse
	FRAMEWORKS
		com.renesas.arm.Arm.CMSIS.CMSIS5.CoreM
		com.renesas.arm.Renesas.Common.fsp_common
		com.renesas.arm.Renesas.BSP.device.mcu
		com.renesas.arm.Renesas.BSP.device.family
		com.renesas.arm.Renesas.BSP.fsp.family
		com.renesas.arm.AWS.RTOS.FreeRTOS.all
		com.renesas.arm.Renesas.HAL_Drivers.r_ioport
	FWCONFIGURATION
		config.bsp.common.heap=0x1000
		config.bsp.common.checking=1
		"com.renesas.ra.device.pins.mode.p408=(uint32_t) IOPORT_CFG_PORT_DIRECTION_OUTPUT | (uint32_t) IOPORT_CFG_PORT_OUTPUT_LOW"
		board.clock.xtal.freq=8000000
		"board.clock.pll.div=(BSP_CLOCKS_PLL_DIV_1)"
		config.awsfreertos.thread.configuse_mutexes=1
		"config.awsfreertos.thread.configlibrary_max_syscall_interrupt_priority=(3)"
		config.awsfreertos.thread.include_hw_stack_monitor=1
	HWREGISTER_LIST_FILE DeviceDefinitions/R7FA4M2AC.xml
	C_STANDARD 99
	DISABLE_GNU_EXTENSIONS)

add_bsp_based_executable(
	NAME EmbeddedProject_Temp1
	SOURCES
		hal_entry.c
		ra_cfg/fsp_cfg/bsp/bsp_cfg.h
		ra_cfg/fsp_cfg/bsp/bsp_pin_cfg.h
		ra_gen/pin_data.c
		ra_gen/common_data.h
		ra_gen/common_data.c
		ra_gen/hal_data.h
		ra_gen/hal_data.c
		ra_cfg/fsp_cfg/bsp/board_cfg.h
		ra_gen/vector_data.h
		ra_gen/vector_data.c
		ra_cfg/fsp_cfg/r_ioport_cfg.h
		ra_cfg/aws/FreeRTOSConfig.h
		ra_cfg/fsp_cfg/bsp/bsp_mcu_device_pn_cfg.h
		ra_cfg/fsp_cfg/bsp/bsp_mcu_device_cfg.h
		ra_cfg/fsp_cfg/bsp/bsp_mcu_family_cfg.h
		ra_gen/bsp_clock_cfg.h
		ra_gen/main.c
		ra_gen/systemStateThread.c
		systemStateThread_entry.c
		ra_gen/systemStateThread.h
		ra_gen/vBatteryStateThread.c
		ra_gen/vBatteryStateThread.h
		vBatteryStateThread_entry.c
	GENERATE_BIN
	GENERATE_MAP)
