#pragma once

#include "bsp_api.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "hal_data.h"
#ifdef __cplusplus
extern "C" void blinky_thread_entry(void * pvParameters);
#else
extern void systemStateThread_entry(void *pvParameters);
#endif
FSP_HEADER
FSP_FOOTER