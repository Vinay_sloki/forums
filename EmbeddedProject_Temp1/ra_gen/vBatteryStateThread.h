#pragma once

#include "bsp_api.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "hal_data.h"
#ifdef __cplusplus
extern "C" void vBatteryStateThread_entry(void * pvParameters);
#else
extern void vBatteryStateThread_entry(void *pvParameters);
#endif
FSP_HEADER
FSP_FOOTER
/* VBATTERYSTATETHREAD_H_ */