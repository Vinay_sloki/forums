#include "systemStateThread.h"

uint8_t Count_g = 0;

/* State Machine entry function */
/* pvParameters contains TaskHandle_t */
void systemStateThread_entry(void *pvParameters)
{
	FSP_PARAMETER_NOT_USED(pvParameters);
	const TickType_t xDelay = 1000 / portTICK_PERIOD_MS;
	while (1)
	{
		Count_g++;
//		Count_g = 0;
		
		vTaskDelay(xDelay);
//		R_BSP_SoftwareDelay(500, BSP_DELAY_UNITS_MILLISECONDS);
	
//		vTaskMissedYield();
//		taskYIELD();
//		vPortYield();
	}
}
